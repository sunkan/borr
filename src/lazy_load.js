/*
 * Build date: @DATE
 * Build version: @VERSION
 * Author: @AUTHOR
 */
/*
 * lazyload.js (c) Lorenzo Giuliani
 * MIT License (http://www.opensource.org/licenses/mit-license.html)
 *
 * expects a list of:
 * `<img src="blank.gif" data-src="my_image.png" width="600" height="400" class="lazy">`
 */
define('Borr/lazy_load', ['Buri/dom', 'Buri/event'], function(dom, event) {
  var 
    images = dom.selectAll('img[data-src]'),
    isProcessing = [],
    id = 1,
    range = 0,
    elementInViewport = function(el) {
      var rect = el.getBoundingClientRect();
      return (
        rect.top    >= 0 &&
        rect.left   >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight)
      );
    },
    loadImage = function(index, fn) {
      var 
        el = images[index],
        img = new Image(),
        src = el.getAttribute('data-src');
      img.onload = function() {
        el.src = src;
        fn && fn(index);
      };
      img.src = src;
    },
    processScroll = function(){
      var
        onLoad = function (index) {
          images.splice(index, 1);
        };
      for (var i = 0; i < images.length; i++) {
        if (!images[i].lazyId) {
          images[i].lazyId = id++;
        }
        if (elementInViewport(images[i])) {
          if (isProcessing[images[i].lazyId]) {
            return;
          }
          isProcessing[images[i].lazyId] = true;
          loadImage(i, onLoad);
        }
      }
    },
    lazyLoad = function(range) {
      range = parseInt(range);
      event.on(window, 'scroll', processScroll);
      event.on(window, 'load', function(){
        processScroll();
      });
    };
  lazyLoad.prototype.refresh = function() {
    images = dom.selectAll('img[data-src]');
  };
  processScroll();
  return lazyLoad;
});
