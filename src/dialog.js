define('Borr/dialog', ['Buri/dom'], function(dom) {
  var dialog = function(el) {
    this.el = el;
  };
  dialog.prototype.hide = function(clb) {
    clb && clb(this.el);
    this.el.style.display = 'none';
    return this;
  };
  dialog.prototype.show = function(clb) {
    clb && clb(this.el);
    this.el.style.display = 'block';
    return this;
  };
  dialog.prototype.find = function(query) {
    return dom.selectAll(query);
  };
  return  dialog;
});