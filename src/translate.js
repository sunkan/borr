define('Borr/translate', ['Buri/util', 'Buri/dom', 'Buri/promise', 'Buri/jsonp', 'Borr/assets'], function(util, dom, Promise, JSONP, assets) {
  "use strict";
  var 
    loading = {},
    currentTranslation,
    _getString = function(key, tree) {
      if (!tree) {
        tree = currentTranslation;
      }
      if (!tree) {
        return key;
      }
      return util.objectGet(key, tree)||key;
    },
    testParamRegExp = /%[sd]/,
    watchList = [],
    watch = function(node, attr, key, args, path) {
      if (!node._twatch) {
        node._twatch = true;
        args = args||[];
        args.splice(0,1, key);
        watchList.push({
          node: node,
          attr: attr,
          key : key,
          args : args,
          path : path||pathKey
        });
      } else {
        watchList.forEach(function(row) {
          if (row.node == node && row.attr == attr) {
            args.splice(0,1, key);
            row.args = args;
            row.key  = key;
            row.path = path||pathKey;
          }
        });
      }
      update(node);
      return node;
    },
    update = function(el) {
      watchList.length&&watchList.forEach(function(row) {
        if (!el || (el && el == row.node)) {
          if (row.path) {
            setCurrent(row.path);
          }
          dom.set(row.node, row.attr, translate.apply(translate, row.args));
        }
      }); 
    },
    translate = function(translationKey) {
      var args = util.toArray(arguments);
      if (translationKey && translationKey.nodeType==1) {
        var collection = util.parseData(translationKey.getAttribute('data-translate'));
        for (var key in collection) {
          var attr = 'html';
          if (collection.hasOwnProperty(key)) {
            if (!util.isInt(key)) {
              attr = key;
            }
            args.splice(0, 1, collection[key]);
            watch(translationKey, attr, collection[key], args);
            dom.set(translationKey, attr, translate.apply(translate, args));
          }
        }
        return key;
      }
      var string = _getString(args.shift());
      if (Array.isArray(string)) {
        string = args[0]===1?string[0]:(args[0]>0?string[1]:(string[2]||string[1]));
      }
      if (testParamRegExp.test(string)) {
        return util.format(string, args);
      }
      return string;
    },
    setCurrent = function(key) {
      if (pathKey != key) {
        if (obj.translations[key] && obj.translations[key][currentLanguage]) {
          currentTranslation = obj.translations[key][currentLanguage];
        }
      }
      pathKey = key;
    },
    basePath = {
      root:'http://files.forme.se/static/language/'
    },
    currentLanguage = 'sv',
    pathKey = 'root',
    obj = {
      translations:{root:{}},
      '_':translate,
      watch: watch,
      update: update,
      setCurrent : setCurrent,
      setBasePath : function(key, path) {
        basePath[key] = path;
      },
      addTranslation : function(lang, translation, key) {
        if (!obj.translations[key||'root']) {
          obj.translations[key||'root'] = {};
        }
        obj.translations[key||'root'][lang] = translation;
      },
      loadLangFile: function(lang, key) {
        var promise = new Promise();
        if (obj.translations[key||pathKey]&&obj.translations[key||pathKey][lang]) {
          currentLanguage = lang;
          pathKey = key||pathKey;
          currentTranslation = obj.translations[key||pathKey][lang];
          promise.resolve(obj);
          update();
        } else {
          assets.loadScript(basePath[key||pathKey]+lang+'.js', function() {
            currentLanguage = lang;
            pathKey = key||pathKey;
            currentTranslation = obj.translations[key||pathKey][lang];
            promise.resolve(obj);
            update();
          });
        }
        return promise;
      },
      setLang : function(lang, clb) {
        var promise = new Promise();
        if (obj.translations[pathKey] && obj.translations[pathKey][lang]) {
          currentLanguage = lang;
          currentTranslation = obj.translations[pathKey][lang];
          clb && clb(obj);
          promise.resolve(obj);
          update();
        } else if (!loading[lang+pathKey]) {
          loading[lang+pathKey] = true;
          JSONP.get(basePath[pathKey]+lang+'.json', {
            success: function(json) {
              if (!obj.translations[pathKey]) {
                obj.translations[pathKey] = {};
              }
              currentLanguage = lang;
              currentTranslation = obj.translations[pathKey][lang] = json;
              clb && clb(obj);
              promise.resolve(obj);
              update();
            }
          });
        }
        return promise;
      }
    };
  return obj;
});