/*
 * Build date: @DATE
 * Build version: @VERSION
 * Author: @AUTHOR
 */

define('Borr/Message',['Borr/assets', 'Buri/dom', 'Buri/event'], function(assets, dom, event){
  "use strict";

  assets.loadCss('http://formecdn.com/static/Borr/forme.message.v3.css');

  var
    doc = document,
    types = ['error', 'info', 'danger', 'success', 'warning', 'grey'],
    box = doc.createElement('div'),
    onHide = function(el){
      var customEl = dom.select('.custom-el',el);
      if (customEl && !dom.hasClass(customEl, 'no-parent')) {
        customEl.style.display='none';
        doc.body.appendChild(customEl);
      }
    },
    setupMessage = function(opts) {
      opts = opts || {};
      var 
        el = box.cloneNode(true),
        type = 'info';
      if (opts.type && types.indexOf(opts.type)!=-1) {
        type = opts.type;
      }
      if (opts.center) {
        dom.addClass(el, 'alert-center');
      }
      if (opts.fixed) {
        dom.addClass(el, 'fixed');
      }
      if (opts.bottom) {
        dom.addClass(el, 'fixed-bottom');
      }
      dom.addClass(el, 'alert-'+type);
      /* jshint -W106 */
      if (opts.auto_hide) {
        /* jshint +W106 */
        setTimeout(function() {
          dom.removeClass(el, 'active');
          setTimeout(function(){
            el.parentNode.removeChild(el);
          },1000);
        },opts.timeout || 1000);
      }
      return doc.body.appendChild(el);
    },
    message = function(msg, opts) {
      var el = setupMessage(opts);
      dom.select('div',el).innerHTML = msg;
      dom.addClass(el, 'active');
    },
    messageEl = function(contentEl, opts) {
      var el = setupMessage(opts);
      contentEl.parentNode && dom.addClass(contentEl,'no-parent');
      dom.addClass(contentEl, 'custom-el');
      dom.select('div',el).appendChild(contentEl);
      contentEl.style.display = 'block';
      dom.addClass(el, 'active');
    };
  box.classList.add('mobile-msg-box', 'alert');
  box.innerHTML='<div></div>';
  event.on(doc.body, 'click:relay(.mobile-msg-box)', function(e) {
    if (dom.hasClass(e.target,'cancel') || (e.target.tagName!='FORM' && !dom.ancestorByTag(e.target, 'form')))  {
      onHide(this);
      dom.removeClass(this, 'active');
      setTimeout(function() {
        this.parentNode.removeChild(this);
      }.bind(this), 1000);
    }
  });

  var obj = {
    raw : function(msg, opts) {
      if (msg && msg.nodeType) {
        messageEl(msg, opts);
      } else {
        message(msg, opts);
      }
    }
  };

  types.forEach(function(method) {
    var key = method[0].toUpperCase()+method.substr(1);
    obj[key] = function(msg, opts) {
      opts = opts || {};
      opts.type = method;
      opts.fixed = true;
      opts.center = true;
      obj.raw(msg, opts);
    };
  });

  return obj;
});