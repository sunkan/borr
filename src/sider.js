/*
 * Build date: @DATE
 * Build version: @VERSION
 * Author: @AUTHOR
 */
define('Borr/sider', ['Buri/dom', 'Borr/assets', 'Buri/dev', 'Borr/pub_sub'], function(dom, assets, dev, radio) {
  var 
    cache = {},
    getEl = function(query) {
      if (typeof query == 'string') {
        return cache[query] || (cache[query]=dom.select(query));
      }
      return query;
    };
  function Sider(opts){
    this.options = opts;
    this.container = opts.container;
  }
  Sider.prototype = {
    constructor:Sider,
    setSide:function(side, el) {
      this.options[side] = el;
    },
    show:function(side) {
      dev.log('show side:',side);
      if (!this.options[side]) {
        return;
      }
      var 
        sideEl=getEl(this.options[side]),
        translate3d='',
        windowInnerWidth = window.innerWidth,
        x=0;
      if (this.currentSide && this.currentSide != sideEl) {
        this.hide(this.currentSide.getAttribute('data-side'));
      }
      this.currentSide = sideEl;
      if (sideEl.getAttribute('data-width') == 'fill') {
        sideEl.style.zIndex = 0;
        translate3d = 'translate3d('+(Math.abs(windowInnerWidth-40)*(side=='left'?1:-1))+'px, 0px, 0px)';
        this.container.setAttribute('style', '-webkit-transform:'+translate3d+';transform:'+translate3d+';');
      } else {
        sideEl.style.zIndex = 0;
        cache.minWidth || (cache.minWidth = parseInt(dom.getStyle(this.container, 'minWidth')));
        if (side!='left') {
          x = (cache.minWidth-(windowInnerWidth-sideEl.clientWidth));
          x = x<0?0:x*-1;
        } else {
          x = Math.abs(sideEl.clientWidth)*1;
        }
        translate3d = 'translate3d('+x+'px, 0px, 0px)';
        this.container.setAttribute('style', 'width:'+(windowInnerWidth-sideEl.clientWidth)+'px !important;transform: '+translate3d+'; -webkit-transform:'+translate3d+';');
      }
      radio.publish('sider.show', {'side':side, 'el':sideEl, 'sider':this});
    },
    hide:function(side) {
      if (!this.options[side]) {
        return;
      }
      var sideEl=getEl(this.options[side]);
      this.container.setAttribute('style', '');
      sideEl.style.zIndex = -1;
      this.currentSide = false;
      radio.publish('sider.hide', {'side':side, 'el':sideEl, 'sider':this});
    },
    toggle:function(side) {
      if (!this.options[side]) {
        return;
      }
      var sideEl = getEl(this.options[side]);
      if (this.currentSide == sideEl) {
        this.hide(side);
      } else {
        this.show(side);
      }
    },
    resize:function() {
      if (!this.currentSide) {
        return ;
      }
      var 
        side = this.currentSide,
        translate3d,
        windowInnerWidth = window.innerWidth,
        isRight = side.getAttribute('data-side')!='left';

      if (side.getAttribute('data-width')=='fill') {
        translate3d = 'translate3d('+(Math.abs(windowInnerWidth-40)*(side=='left'?1:-1))+'px, 0px, 0px)';
        this.container.setAttribute('style', '-webkit-transform:'+translate3d+';transform:'+translate3d+';');
      } else {
        var width = windowInnerWidth-side.clientWidth,
            left = isRight?cache.minWidth-width:side.clientWidth;

        isRight && (left = left<0?0:left);
        translate3d = 'translate3d('+Math.abs(left)*(isRight?-1:1)+'px, 0px, 0px)';
        this.container.setAttribute('style', 'width:'+width+'px !important;transform: '+translate3d+'; -webkit-transform:'+translate3d+';');

      }
      radio.publish('sider.resize', [{'el':side,'sider':this}]);
    }
  };
  return Sider;
});