define('Borr/radio',['Buri/util', 'Buri/dom', 'Buri/event', 'Buri/storage'], function(util, dom, event, storage){
  var
    getStore = storage.getStore,
    valueField = 'data-value',
    inputIdField = 'data-input',
    cssClasses = 'btn-primary btn-info btn-success btn-warning btn-danger btn-inverse btn-link'.split(' '),
    buildRadio = function(el) {
      var 
        inputId = el.getAttribute('id'),
        label = inputId ? dom.select('label[for='+inputId+']') : dom.ancestor(el, 'label'),
        settings = util.parseData(el.getAttribute('data-settings')),
        onClass = settings.onClass||'inverse',
        group = util.stringToElement('<div class="btn-group" data-type="radio" data-on-class="'+onClass+'"></div>'),
        onBtn = util.stringToElement('<button class="btn '+(el.checked?'btn-'+onClass:'')+'" '+valueField+'="on">'+settings.onText+'</button>'),
        offBtn = util.stringToElement('<button class="btn '+(el.checked?'':'btn-'+onClass)+'" '+valueField+'="off">'+settings.offText+'</button>');
      
      if (settings.onText.indexOf('t.')!==-1) {
        onBtn.setAttribute('data-translate', settings.onText.substr(2));
      }
      if (settings.offText.indexOf('t.')!==-1) {
        offBtn.setAttribute('data-translate', settings.offText.substr(2));
      }
      group.appendChild(onBtn);
      group.appendChild(offBtn);

      getStore(group).set('input', el);

      event.on(group, 'click:relay(.btn)', clb);
      dom.inject((label && label.contains(el)?label:el), group, 'after');

      getStore(el).set('radio', group);
      dom.setStyle(el, 'display','none');
      event.on(el, 'change', function(e) {
        dom.removeClass(onBtn, 'btn-'+onClass);
        dom.removeClass(offBtn, 'btn-'+onClass);
        var btn = (el.checked ? onBtn : offBtn),
          changeEvent = new CustomEvent('change',{btn:btn, value:btn.getAttribute(valueField)});
        dom.addClass(btn, 'btn-'+onClass);
        group.dispatchEvent(changeEvent);
      });
      if (settings.hideLabel == 'true') {
        label && dom.setStyle(label, 'display', 'none');
      }
      return group;
    },
    clb = function(e) {
      e && e.preventDefault();
      e && e.stopPropagation();

      var 
        target = dom.hasClass(this,'btn')?this:e.target,
        parent = dom.ancestor(target, '[data-type]'),
        onClass = 'btn-'+(parent.getAttribute('data-on-class') || 'inverse'),
        input;
      if (dom.hasClass(target, onClass )) {
        return ;
      }
      dom.removeClass(dom.getSiblings(target, '.btn'), onClass);
      dom.addClass(target, onClass);
      if ( (input = getStore(parent).get('input')) ||
           (parent.getAttribute(inputIdField) &&
              (input = dom.select(parent.getAttribute(inputIdField)))
           )
         ) {
        input.checked = (target.getAttribute(valueField)=='on');
      }
      parent.dispatchEvent(new CustomEvent('change',{btn:target, value:target.getAttribute(valueField)}));
    },
    defaultOptions = {
      element : document,
      search:'[data-type=radio], [data-type=checkbox]',
      initGroups: true
    },
    radio;

  radio = function(options) {
    if (options && options.nodeType) {
      buildRadio(options);
      return;
    }

    options = util.extend(options, defaultOptions);
    var element = options.element;
    if (options.initGroups) {
      var 
        groups = dom.selectAll(options.search, element),
        btns = dom.selectAll('['+inputIdField+'] .btn', element),
        toggle = function() {
          var btns = dom.selectAll('.btn', this);
          for(var i = 0, l=btns.length; i < l; i++) {
            if (!dom.hasClass(btns[i], 'btn-'+(btns[i].getAttribute('data-on-class') || 'inverse'))) {
              clb.bind(btns[i])();
              break;
            }
          }
        };
      event.on(document, 'click:relay('+options.search+' .btn)', clb);

      groups.forEach(function(el) {
        event.on(el, 'toggle', toggle);
      });
      dom.addClass(groups, 'done-building');
      if (btns && btns.length) {
        cssClasses.forEach(function(cls) {
          dom.removeClass(btns, cls);
        });

        dom.selectAll('['+inputIdField+']', element).forEach(function(el) {
          var 
            inputField = dom.select( el.getAttribute(inputIdField) , element),
            onClass = 'btn-'+(el.getAttribute('data-on-class') || 'inverse'),
            btn = dom.select('['+valueField+'='+(inputField.checked?'on':'off')+']', el);
          if (btn) {
            dom.addClass(btn, onClass);
          }
          dom.setStyle(inputField, 'display', 'none');

        });
      }
    }
    /* jshint -W106 */
    if (options.auto_convert) {
      /* jshint +W106 */
      dom.selectAll('input[data-build=radio], input[data-build=radio]', element).forEach(function (el) {
        buildRadio(el);
      });
    }
  };

  return radio;
});