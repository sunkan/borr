define('Borr/assets', ['Buri/util', 'Buri/dom', 'Buri/xhr', 'Buri/promise'], function(util, dom, xhr, Promise){
  var 
    doc = document, 
    head = dom.select('head'),
    _loadedScripts = [],
    _loadingScripts = [],
    _loadedCss = [],
    _loadingCss = [],
    _hashCode = util.hashCode,
    stringEndsWith = util.stringEndsWith,
    assets = {
      loadTemplate : function(url, clb, multi) {
        var loaded = new Promise();
        xhr.get(url, {
          success:function(html) {
            var 
              container = doc.createElement('div'),
              successFn = function(data) {
                this.value = data.token;
              },
              els;
            container.innerHTML = html;
            els = dom.selectAll('input[data-token]', container);
            if (els.length) {
              for (var i = els.length - 1; i >= 0; i--) {
                xhr.get('/js.proxy.php?rt=87214&rtn='+els[i].getAttribute('data-token'), {
                  format:'jsend',
                  success:successFn.bind(els[i])
                });
              }
            }
            if (multi === true) {
              var templateStore={
                '_container':container
              };
              dom.selectAll('[data-template]', container).forEach(function(tpl) {
                templateStore[tpl.getAttribute('data-template')] = tpl;
              });
              loaded.resolve(templateStore);
            } else {
              loaded.resolve(container);
              clb(dom.select('.x-template', container));
            }
          }
        });
        return loaded;
      },
      load : function(urls, clb) {
        var
          count = 0,
          countClb = function(){
            count++;
            if (count == urls.length) {
              clb.call();
            }
          },
          i = urls.length - 1;

        for (; i >= 0; i--) {
          var hasQuery = urls[i].indexOf('?'),
          file = hasQuery > 0?urls[i].substring(0, hasQuery):urls[i];
          if (stringEndsWith(file, '.css')) {
            assets.loadCss(urls[i], countClb);
          } else if (stringEndsWith(file, '.js')) {
            assets.loadScript(urls[i], countClb);
          }
        }
      },
      loadCss : function(file, clb) {
        var fileKey = file.replace(/(\.v[0-9]+)(\.js)(\?(.*)+)/,'$2');
        if (_loadedCss.indexOf(fileKey) === -1 && _loadingCss.indexOf(fileKey) === -1) {
          setTimeout(function(){
            _loadingCss.push(fileKey);

            var img = doc.createElement('img'),
              link = doc.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = file;
            link.id = 'css-'+_hashCode(fileKey);

            head.appendChild(link);

            img.onerror = function(){
              _loadingCss.splice(_loadingCss.indexOf(fileKey), 1);
              _loadedCss.push(fileKey);
              clb && clb.call();
            };
            img.src = file;
          }, 0);
        } else if (_loadedCss.indexOf(fileKey) !== -1) {
          clb && clb();
        }
        return true;
      },
      loadScript : function(file, clb) {
        var fileKey = file.replace(/(\.v[0-9]+)(\.js)(\?(.*)+)/,'$2');
        if (_loadedScripts.indexOf(fileKey) === -1 && _loadingScripts.indexOf(fileKey) === -1) {
          setTimeout(function(){
            _loadingScripts.push(fileKey);

            var 
              script = doc.createElement('script'),
              load = function(){
                if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                  _loadingScripts.splice(_loadingScripts.indexOf(fileKey), 1);
                  _loadedScripts.push(fileKey);
                  clb && clb();
                }
              };
            script.id = 'js-'+_hashCode(fileKey);
            script.src = file;
            script.async = true;
            script.onload = script.onreadystatechange = load;
            script.type = 'text/javascript';
            head.appendChild(script);
          }, 0);
        } else if (_loadedScripts.indexOf(fileKey) !== -1) {
          clb && clb();
        } else if (_loadingScripts.indexOf(fileKey) !== -1) {
          setTimeout(function(){
            assets.loadScript(file, clb);
          }, 10);
        }
      }
    };
  return assets;
});