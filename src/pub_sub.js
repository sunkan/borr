define('Borr/pub_sub', ['Buri/util', 'Buri/dom', 'Buri/xhr'], function(util, dom, xhr){
  var 
    pubsubTopics = {},
    pubsubId = -1,
    pubSub = {
      subscribe : function (topic, fn) {
        if (!pubsubTopics[topic]) {
          pubsubTopics[topic] = [];
        }
        // Create a unique id to be associated with the subscriber
        var token = (++pubsubId).toString();

        // Store the token and the subscriber (function) in this topic
        pubsubTopics[topic].push({
          token: token,
          fn: fn
        });

        return token;
      },
      unsubscribe : function (token) {
        for (var m in pubsubTopics) {
          if (pubsubTopics[m]) {
            for (var i = 0, j = pubsubTopics[m].length; i < j; i++) {
              if (pubsubTopics[m][i].token === token) {
                pubsubTopics[m].splice(i, 1);

                return token;
              }
            }
          }
        }

        return false;
      },
      publish : function (topic, data, sync) {
        if (topic!='*' && pubsubTopics['*']) {
          pubSub.publish('*', data);
        }
        // If the topic specified cannot be found then return the function early (no point continuing)
        if (!pubsubTopics[topic]) {
          return false;
        }
        var loop = function(){
          // Cache the topic
          var subscribers = pubsubTopics[topic],
            len = pubsubTopics[topic].length;

          while (len--) {
            subscribers[len].fn(topic, data);
          }
        };

        if (sync) {
          loop();
        } else {
          // Asynchronously execute each subscriber function
          setTimeout(loop, 0);
        }

        return true;
      }
    };
  return pubSub;
});