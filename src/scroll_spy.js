/*
---
description: ScrollSpy

authors:
- David Walsh (http://davidwalsh.name)

license:
- MIT-style license

requires:
core/1.2.1: '*'

provides:
- ScrollSpy
...
*/
define('Borr/scroll_spy',['Buri/util', 'Buri/event'], function(util, event) {
  var 
    defaultOptions = {
      container: window,
      max: 0,
      min: 0,
      mode:'vertical'
      /*
      onEnter: $empty,
      onLeave: $empty,
      onScroll: $empty,
      onTick: $empty
      */
    },
    scrollSpy = function(options) {
      this.options = util.extend(options||{}, defaultOptions);
      this.container = this.options.container;
      this.enters = 0;
      this.leaves = 0;
      this.inside = false;
      var self = this;
      this.listener = function(e) {
        /* if it has reached the level */
        var position = {
            x: (self.container==window?document.body:self.container).scrollWidth, 
            y: (self.container==window?document.body:self.container).scrollHeight
          },
          xy = position[self.options.mode == 'vertical' ? 'y' : 'x'];
        /* if we reach the minimum and are still below the max... */
        if(xy >= self.options.min && (self.options.max === 0 || xy <= self.options.max)) {
          /* trigger enter event if necessary */
          if(!self.inside) {
            /* record as inside */
            self.inside = true;
            self.enters++;
            /* fire enter event */
            self.trigger('enter',[position,self.enters,e]);
          }
          /* trigger the "tick", always */
          self.trigger('tick',[position,self.inside,self.enters,self.leaves,e]);
        }
        /* trigger leave */
        else if(self.inside){
          self.inside = false;
          self.leaves++;
          self.trigger('leave',[position,self.leaves,e]);
        }
        /* fire scroll event */
        self.trigger('scroll',[position,self.inside,self.enters,self.leaves,e]);
      };

      /* make it happen */
      this.addListener();
    };
  scrollSpy.prototype.addListener = function() {
    this.start();
  };
  scrollSpy.prototype.start = function() {
    event.on(this.container, 'scroll', this.listener);
  };
  scrollSpy.prototype.stop = function() {
    event.off(this.container, 'scroll', this.listener);
  };
  event.extendObject(scrollSpy.prototype);

  return scrollSpy;
});