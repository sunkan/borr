/* =============================================================

  Smooth Scroll 2.3
  Animate scrolling to anchor links, by Chris Ferdinandi.
  http://gomakethings.com

  Free to use under the MIT License.
  http://gomakethings.com/mit/

 * ============================================================= */

define('Borr/smooth_scroll', [], function() {
  'use strict';
  // Function to animate the scroll
  var smoothScroll = function (anchor, duration) {
    // Calculate how far and how fast to scroll
    var startLocation = window.pageYOffset,
      endLocation = anchor.offsetTop,
      distance = endLocation - startLocation,
      increments = distance/(duration/16),
      stopAnimation,
      runAnimation,
      // Scroll the page by an increment, and check if it's time to stop
      animateScroll = function () {
        window.scrollBy(0, increments);
        stopAnimation();
      };

    // If scrolling down
    if ( increments >= 0 ) {
      // Stop animation when you reach the anchor OR the bottom of the page
      stopAnimation = function () {
        var travelled = window.pageYOffset;
        if ( (travelled >= (endLocation - increments)) || ((window.innerHeight + travelled) >= document.body.offsetHeight) ) {
          clearInterval(runAnimation);
        }
      };
    }
    // If scrolling up
    else {
      // Stop animation when you reach the anchor OR the top of the page
      stopAnimation = function () {
        var travelled = window.pageYOffset;
        if ( travelled <= (endLocation || 0) ) {
          clearInterval(runAnimation);
        }
      };
    }

    // Loop the animation function
    runAnimation = setInterval(animateScroll, 16);
  };

  return smoothScroll;
});