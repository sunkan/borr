define('Borr/lightbox', ['Buri/util', 'Buri/dom', 'Buri/event', 'Borr/assets'], function(util, dom, event, assets) {
  var 
    body = document.body,
    createTarget = function(baseCls) {
      var el = util.stringToElement('<div aria-hidden="true" class="'+baseCls+'-lightbox-target"><img/><a class="'+baseCls+'-lightbox-close" href="#"></a></div>');
      body.appendChild(el);
      return el;
    };
  var lightbox = function(opts) {
    assets.loadCss(opts.css||'http://formecdn.com/static/Borr/lightbox.css');
    var 
      target = dom.select('.borr-lightbox-target') || createTarget('borr'),
      img = dom.select('img', target);

    event.on(body, 'click:relay(a[data-type=lightbox])', function(e) {
      e && e.preventDefault();
      img.src = this.href;
      target.removeAttribute('aria-hidden');
    });
    event.on(target, 'click:relay([data-lightbox=close])', function(e) {
      e && e.preventDefault();
      target.setAttribute('aria-hidden', 'true');
    });
    event.on(target, 'click', function(e) {
      if (e && e.target.tagName.toLowerCase() !='img') {
        e && e.preventDefault();
        img.removeAttribute('src');
        target.setAttribute('aria-hidden', 'true');
      }
    });
  };
  return lightbox;
});