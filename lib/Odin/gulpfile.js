var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    replace = require('gulp-replace'),
    closure_compiler = require('gulp-closure-compiler'),
    stylish = require('jshint-stylish');


gulp.task('odin', function() {
  return gulp.src('src/Odin/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter(stylish))
    .pipe(concat('odin.js'))
    .pipe(gulp.dest('build'))
    .pipe(closure_compiler())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('build'));
});

gulp.task('clean', function() {
  return gulp.src(['odin'], {read: false})
    .pipe(clean());
});

gulp.task('default', ['clean'], function() {
    gulp.start('odin');
});
gulp.task('watch', function() {

  // Watch .js files
  gulp.watch('src/*.js', ['odin']);
});