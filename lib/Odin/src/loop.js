define('Odin/loop', ['Odin/index', 'Buri/dom'], function(Odin, dom) {

  Odin.registerAttr('data-bind-loop', function(el, data, tpl) {
    var key = el.getAttribute('data-bind-loop'),
      localData = tpl.get(key, data),
      dataLength = localData.length,
      tplRaw = document.createDocumentFragment();
    dom.getChildren(el).forEach(function(child) {
      tplRaw.appendChild(child);
    });
    el.innerHTML = '';

    for (var i = 0; i < dataLength; i++) {
      var renderRow = tplRaw.cloneNode(true);
      if (i==(dataLength-1)) {
        localData[i].last = true;
      }
      localData[i].parent = data;
      renderRow = tpl.render(renderRow, localData[i]);
      el.appendChild(renderRow);
    }
    el.removeAttribute('data-bind-loop');
    return el;
  });
});