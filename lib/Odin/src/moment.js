define('Odin/moment', ['Odin/index', 'Buri/util', 'Buri/dom', 'moment'], function(Odin, util, dom, moment) {

  Odin.registerAttr('data-bind-moment', (function() {
    var 
      momentEls = [],
      momentInit = false,
      startupTimer,
      handleMoment = function(el, value) {
        var format = el.getAttribute('data-moment'),
          m = util.isInt(value)?moment.unix(value):moment(value);
        if (format === 'LLL') {
          if (moment().diff(m, 'days') < 1) {
            format = 'fromNow';
          }
        }
        if (m[format]) {
          value = m[format]();
        } else {
          value = m.format(format);
        }
        return value;
      },
      update = function() {
        if (moment) {
          momentInit = true;
          momentEls.forEach(function(row) {
            dom.set(row.el, row.attr, handleMoment(row.el, row.value));
          });
        }
        if (momentInit && startupTimer) {
          clearTimeout(startupTimer);
        }
      };

    return function(el, data, tpl) {
      var bind = util.parseData(el.getAttribute('data-bind-moment'));
      el.removeAttribute('data-bind-moment');
      for (var key in bind) {
        var attr = 'html',value, old;
        if (bind.hasOwnProperty(key)) {
          if (!util.isInt(key)) {
            attr = key;
          } else if (el.tagName.toLowerCase()=='input') {
            attr = 'value';
          }
          value = tpl.get(bind[key], data);
          momentEls.push({el:el, value:value, attr:attr});
          if (moment) {
            value = handleMoment(el, value);
          } else {
            startupTimer = setInterval(update, 500);
          }
          if (el.hasAttribute('data-moment-update')) {
            setInterval(update, el.getAttribute('data-moment-update')||60000);
          }
          if ( (old = dom.get(el, attr)) && old.indexOf('{')) {
            var td = {};
            td[(bind[key].split('.').pop())] = value;
            value = util.template(old, td);
          }
          dom.set(el, attr, value);
        }
      }
      return el;
    };
  })());
});