define('Buri/jsonp', ['Buri/util', 'Buri/dom', 'Buri/promise'], function(util, dom, Promise){
  "use strict";
  var id = 0,
    head = dom.select("head");

  return {
    get:function(url, options) {
      id++;
      var
        promise = new Promise(),
        data = options.data || {},
        key = options.key || 'callback',
        clbName = 'buri_jsonp_clb_'+id,
        timeout = options.timeout || 10,
        timeoutTimer,
        running = false,
        script = document.createElement('script');

      promise.then(options.success || function(){}, options.fail||options.error);

      data[key] = clbName;
      script.type = "text/javascript";
      script.async = true;
      script.src = url + (url.indexOf("?")+1 ? "&" : "?") + util.toQueryString(data);
      window[clbName] = function(json) {
        if (!running) {
          return;
        }
        running = false;
        head.removeChild(script);
        delete window[clbName];
        if (timeoutTimer) {
          clearTimeout(timeoutTimer);
          if (options.format == 'jsend') {
            if (json.status != 'success') {
              return promise.reject(json.data||json);
            }
          }
          promise.resolve(json);
        }
      };

      timeoutTimer = window.setTimeout(function(){
        running = false;
        promise.reject({message:'timeout'});
      }, timeout * 1000);
      running = true;
      head.appendChild(script);

      promise.isRunning = function() {
        return running;
      };
      promise.cancel = function() {
        running = false;
        head.removeChild(script);
        script=null;
        delete window[clbName];
        if (timeoutTimer) {
          clearTimeout(timeoutTimer);
        }
        promise.reject({'message':'cancel'});
      };

      return promise;
    }
  };
});