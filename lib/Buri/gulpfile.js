var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    replace = require('gulp-replace'),
    closure_compiler = require('gulp-closure-compiler'),
    stylish = require('jshint-stylish');

gulp.task('buri', function() {
  return gulp.src([
        'src/init.js',
        'src/dev.js',
        'src/class.js',
        'src/util.js',
        'src/storage.js',
        'src/promise.js',
        'src/dom.js',
        'src/event.js',
        'src/xhr.js',
        'src/jsonp.js',
        'src/selector_listner.js'
    ])
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter(stylish))
    .pipe(gulp.dest('build'))
    .pipe(concat('buri.js'))
    .pipe(gulp.dest('build'))
    .pipe(closure_compiler())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('build'));
});

gulp.task('clean', function() {
  return gulp.src(['build'], {read: false})
    .pipe(clean());
});

gulp.task('default', ['clean'], function() {
    gulp.start('buri');
});
gulp.task('watch', function() {
  // Watch .js files
  gulp.watch('src/*.js', ['buri']);
});